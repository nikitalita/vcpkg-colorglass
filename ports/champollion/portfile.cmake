vcpkg_from_github(
  OUT_SOURCE_PATH SOURCE_PATH
  REPO Orvid/Champollion
  REF v1.1.0
  SHA512 fa8247bf08569571ab580daa996163bf26480e2e3cc23513e8c08f7197f14d725224774be9cdc133ed3336b8225c7fb0981a77e02908df83cb251240bbea0006
  HEAD_REF master
)
# Check if one or more features are a part of a package installation.
# See /docs/maintainers/vcpkg_check_features.md for more details
vcpkg_check_features(OUT_FEATURE_OPTIONS FEATURE_OPTIONS
  INVERTED_FEATURES
	  standalone   CHAMPOLLION_STATIC_LIBRARY
)
vcpkg_cmake_configure(
    SOURCE_PATH "${SOURCE_PATH}"
		OPTIONS
      ${FEATURE_OPTIONS}
)

vcpkg_cmake_install()
if (${CHAMPOLLION_STATIC_LIBRARY})
  vcpkg_cmake_config_fixup(PACKAGE_NAME Champollion CONFIG_PATH lib/cmake/Champollion)
endif()
file(
        INSTALL "${SOURCE_PATH}/LICENSE"
        DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}"
        RENAME copyright)
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
